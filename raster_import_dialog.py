# -*- coding: utf-8 -*-
"""
/***************************************************************************
 raster2postgis
 QGIS plugin to import raster files to a postgis database
                              -------------------
        begin                : 2019-09-20
        copyright            : (C) 2019 by Geomaster
        email                : geral@geomaster.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2 as     *
 *   published by the Free Software Foundation.                            *
 *                                                                         *
 ***************************************************************************/
"""
import os
from osgeo import gdal,osr
import re
import traceback
import math
from unidecode import unidecode

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog, QDialogButtonBox
from PyQt5.QtCore import Qt, pyqtSlot, QThread, pyqtSignal
from PyQt5.QtGui import QCursor, QIcon

from qgis.core import QgsCoordinateReferenceSystem

from . import qgis_configs
from .upload_raster import PostgisUploader
from .upload_raster import PostgisUtils


FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'ui/raster_import_dialog.ui'))


class RasterImportDialog(QDialog, FORM_CLASS):
    def __init__(self, iface, parent=None):
        super(RasterImportDialog, self).__init__(parent)
        self.initialized = False
        self.setupUi(self)
        self.iface = iface

        self.widget.setHidden(True)
        self.buttonBox.button(
            QDialogButtonBox.Apply).clicked.connect(self.process)

    def showEvent(self, event):
        super(RasterImportDialog, self).showEvent(event)

        self.importProcess = None
        self.uploadList = []

        if not self.initialized:
            self.fillDataSources()
            self.initialized = True

    def fillDataSources(self):
        self.connCombo.clear()
        dblist = qgis_configs.listDataSources()
        firstRow = 'Select connection...' if len(dblist) > 0 else 'No available connections'
        secondRow = 'Refresh connections...'
        self.connCombo.addItems([firstRow, secondRow] + dblist)

    def getConnection(self):
        return self.connCombo.currentText()

    def urlify(self, s):
        # Remove all non-word characters (everything except numbers and letters)
        s = re.sub(r"[^\w\s]", '', s)

        # Replace all runs of whitespace with a single dash
        s = re.sub(r"\s+", '-', s)

        return s

    def getMetadata(self, filename):
        if os.path.isfile(filename):
            ds = gdal.Open(filename)
            prj = ds.GetProjection()
            crs = QgsCoordinateReferenceSystem(prj)
            if crs.isValid():
                # srid = crs.postgisSrid()      # 3763
                srid = crs.authid()             # EPSG:3763
                self.plainTextEdit.appendPlainText('Detected source CRS: {0}'.format(srid))
                self.sridName.setCrs(crs)
                self.pSridName.setCrs(crs)
            else:
                self.plainTextEdit.appendPlainText('Warning: No CRS detected.')

            try:
                # get aux files
                self.uploadList = ds.GetFileList()

                # suggest table name
                tname = os.path.splitext(os.path.basename(filename))[0]
                tname = unidecode(self.urlify(tname))
                tname = 'r2pg_' + tname if tname[0].isdigit() else tname
                re.sub(r'[^$]', '', tname)
                tname = tname[3:] if tname.startswith('pg_') else tname
                tname = tname[:63] if len(tname) > 63 else tname
                self.outTableName.setText(tname.lower())
            except Exception:
                self.rasterFilePath.clear()
                self.plainTextEdit.appendPlainText('Error: Invalid file type')
            finally:
                del ds

    @pyqtSlot('PyQt_PyObject', name='writeText')
    def writeText(self, text):
        self.plainTextEdit.appendPlainText(text)

    def process(self):
        fileName = str(self.rasterFilePath.filePath())
        outSchema = str(self.outSchemaName.currentText())
        outTable = str(self.outTableName.text())

        # validate epsg
        if not self.sridName.crs().isValid():
            self.plainTextEdit.appendPlainText('Error: Invalid source srid')
            return
        if not self.pSridName.crs().isValid():
            self.plainTextEdit.appendPlainText(
                'Error: Invalid projection srid')
            return
        if not outSchema.strip():
            self.plainTextEdit.appendPlainText(
                'Error: Invalid schema')
            return
        if not os.path.isfile(fileName):
            self.plainTextEdit.appendPlainText(
                'Error: Invalid filename')
            return

        conString = qgis_configs.getConnString(self, self.getConnection())

        epsg = self.sridName.crs().authid()
        pepsg = self.pSridName.crs().authid()
        nOver = self.nOverviews.value()
        blockSizeX = str(self.blckSizeX.value())
        blockSizeY = str(self.blckSizeY.value())
        isExternal = self.fsRasterCheckBox.isChecked()

        self.importProcess = rasterImportProcess(conString, fileName, outSchema, outTable, epsg,
                                                 pepsg, blockSizeX, blockSizeY, nOver, isExternal,
                                                 self.uploadList, self.setupDBCheckBox.isChecked())

        self.importProcess.signal.connect(self.writeText)
        self.importProcess.finished.connect(self.finishedImportRaster)

        # TODO
        # Notification: import started instead of change cursor
        # QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        self.iface.messageBar().pushMessage("raster2postgis import started.")
        self.importProcess.start()

    def finishedImportRaster(self):
        # TODO
        # Notification: import successfull
        self.iface.messageBar().pushMessage("raster2postgis import finished.")
        self.plainTextEdit.appendPlainText(
            "Import finished.\n--\n")
        QApplication.restoreOverrideCursor()

    def changeConn(self, newConnName):
        self.outSchemaName.clear()

        if newConnName == 'Refresh connections...':
            self.fillDataSources()
            self.plainTextEdit.appendPlainText("Postgis connections reloaded")
            self.connCombo.setCurrentIndex(0)
        else:
            if newConnName != 'Select connection...' and newConnName != 'No available connections' and newConnName != "":
                conString = qgis_configs.getConnString(self, newConnName)
                # self.plainTextEdit.appendPlainText(
                #     "New connection to: {0}\n".format(conString))
                utils = PostgisUtils(self, conString)
                try:
                    support = utils.checkPostgisRasterSupport()
                    schemas = utils.read_db_schemas()
                    # self.plainTextEdit.appendPlainText(
                    #     "Schemas: {0}\n".format(','.join(schemas)))
                    self.outSchemaName.addItems(schemas)
                except ValueError as error:
                    self.plainTextEdit.appendPlainText(
                        "Error: {0}\n".format(error))


    def reject(self):
        if self.importProcess is not None and self.importProcess.isRunning() and self.importProcess.importer is not None:
            self.importProcess.importer.cancel()
        else:
            self.plainTextEdit.clear()
            super(RasterImportDialog, self).reject()


class rasterImportProcess(QThread):
    signal = pyqtSignal('PyQt_PyObject')

    def __init__(self, conString, fileName, outSchema, outTable, epsg, pepsg, blockSizeX, blockSizeY, nOver, isExternal, uploadList, setupDB):
        QThread.__init__(self)

        self.conString = conString
        self.outSchema = outSchema
        self.outTable = outTable

        self.file = fileName
        self.uploadList = uploadList
        self.isExternal = isExternal

        self.epsg = epsg
        self.pepsg = pepsg
        self.tileSize = blockSizeX+"x"+blockSizeY
        self.overviews = ','.join(
            list(map(lambda x: f"{math.pow(2, x):.0f}", range(1, nOver+1))))

        self.importer = None
        self.setupDB = setupDB

    def write(self, text):
        self.signal.emit(text)

    def run(self):
        try:
            # Start uploading session
            self.write("Connecting to database...")
            remotePath = '' if self.isExternal else '/tmp/'
            self.importer = PostgisUploader(self, self.file, self.conString, self.outSchema, self.outTable, self.epsg,
                                            self.pepsg, self.tileSize, self.overviews, remotePath, self.isExternal, self.uploadList)
            self.write("Connection successful")

            # Check if setup DB
            if self.setupDB:
                self.write("Setting up database")
                self.importer.setupDB()
                self.write('Setup finished')

            # Store raster file
            self.write("Uploading raster files")
            self.importer.storeRaster()
            self.write('Upload finished')

            # call raster2pgsql
            self.write('Setting up raster in postgis')
            self.importer.raster2pgsql()
            self.write('Importing raster finished')
        except Exception as e:
            self.write("Failed.")
            self.write(traceback.format_exc())
            self.write(("Exception: {}".format(e)))
            if self.importer:
                self.write("Applying rollbacks")
                self.importer.doRollback()
        finally:
            if self.importer:
                self.write("Closing uploading session.")
                self.importer.exit()
