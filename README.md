# raster2postgis

## Goal

This plugin allows you to import raster files to a postgis database from QGIS. 
Rasters on Postgis can be added to QGIS projects using DB Manager or the Browser.

## How it works

`raster2postgis` plugin uploads the raster to the server. Then it runs the Postgis utility `raster2pgsql` on 
the server side to insert the raster into the database.

## Installation

You can install the raster2postgis plugin from the menu Plugins →  Manage and Install Plugins...

![](images/raster2postgis-after-install.png)

After installation, the raster2postgis plugin will be available on the Database menu.

![](images/raster2postgis-on-database-menu.png)

### Dependencies

This plugin requires the `unidecode` Python module (to strip accents from filenames if present).

If you are using OSGeo4W to install/update QGIS in Windows, you can install `unidecode` Python module using the OSGeo4W Shell. Open the OSGeo4W Shell and type:
- `py3_env`
- `python -m pip install unidecode`

The following print screen shows the installation of dependencies using OSGeo4W Shell. 
You don't have to start the OSGeo4W Shell as Administrator.

![](images/osgeo4w shell install unidecode.png)

On Linux, you can install the `unidecode` module with `pip install unidecode` (or `pip3 install unidecode`).
You don't have to be root to do it.

If you are unable to install the `unidecode` Python module using the previous methods, you can try to install it directly 
from QGIS Python console. On QGIS, open the Python console and type:
- `import pip`
- `pip.main([ 'install', 'unidecode' ])`

The following print screen shows the installation using QGIS Python console.

![](images/qgis python console.png)

## First run

### Start with small rasters

_Start using a small raster_ and make sure the plugin is working and you get the expected result.
After you fill comfortable with the plugin functionality, you can try larger rasters.
Large rasters will take some time to upload and lod into the database. 
This plugin was not design to manage large rasters.

### First run requirement

For the first run, you need to connect to Postgresql using an account 
with Postgresql `superuser` privilege.

To allow non superuser accounts to use the plugin after the first run, 
you have to select the **Advanced Options**, and then check the option: 
**Create PGSQL functions for other users (if you are superuser)**

![](images/create%20PLSQL%20functions%20for%20non%20superuser%20accounts.png)

You have to use this **Create PGSQL functions for other users** for every database. If you change the database, the required server side functions must be created on each database.

### Fill the form

To run the plugin, you have to provide:
* a raster file (GDAL supported raster)
* the database connection (previously created in QGIS)
* select the schema
* select or accept the table name (created after the raster name)
* the source CRS (if not detected by QGIS)
* the target CRS (select a different one, if you want to reproject the raster)

![](images/form%20required%20data.png)

As you can see in the image above, the raster CRS was detected. 
There is always a notification about the CRS of the source, even if the source CRS is not detected.

The suggested table on Postgresql is computed from the raster name (replacing spaces, accents and so on). If the raster starts with numbers, 
the prefix `r2pg_` is used to make sure the table name does not start with numeric digits.

### Run!

To start the upload process, hit the **Apply** button. 

There is a regular notification in QGIS main window.

![](images/start-notification.png)

When the process finish, you will receive another notification, saying that the process has ended.

_Meanwhile, you can continue to work on QGIS._ The upload can take some time (even hours, for a large raster), but it does not prevents you from working on QGIS. 
The plugins core functionality runs on an thread independent from the QGIS application. 

At any moment, you can cancel the upload, by clicking on Cancel. Cancel will roolback the procees and it should be safe.

There are some details about the process in the plugins dialog. If some error occurs, it will be shown on the dialog.

![](images/raster2postgis_done.png) 

If the upload was successful, you can add the preview the raster in the Browser or in DB Manger.

Preview using DB Manager

![](images/db%20manger%20preview%20raster.png)

Preview using Browser

![](images/browser%20preview%20raster.png)

### Overview tables

With the default options, three additional overview tables are created to speed up the raster rendering on QGIS. 
QGIS will tak advantage of these overview tables. For large rasters, you should increase the number of overviews.

The overview tables are named `o_<factor>_<raster>`, where `<factor>` is the overview factor (2, 4, 8, 16, 32, etc) 
and `<raster>` is the main raster table name.

The following picture shows one raster table `gualtar` with 3 additional overview 
tables (`o_2_gualtar`, `o_4_gualtar`, `o_8_gualtar`).
You just need to add the main table to QGIS.

![](images/raster%20table%20and%20overviews.png)

## Server requirements

The PostgreSQL server must have support for rasters installed. The required environment variables can be set in `/etc/postgresql/13/main/environment` with:
```
POSTGIS_ENABLE_OUTDB_RASTERS=1
POSTGIS_GDAL_ENABLED_DRIVERS=ENABLE_ALL
```

The extensions `postgis` and `postgis_raster` must be installed in the target database. 

This plugin uses Postgis `raster2pgsql` utility on the server side. This must be installed on database server. 
It can be installed with `sudo apt install postgis` on a Debian/Ubuntu server.

If you have Postgis installed from source and multiple versions of PostgreSQL on the server, you have to create a symbolic link for the desired `raster2pgsql` version on a folder on the default PATH, like:
- `sudo ln -s /usr/lib/postgresql/12/bin/raster2pgsql /usr/local/bin/`

### About out-of-db rasters

Rasters can be stored in _db_ or _out-of-db_. If you are not aware of these technical issues, 
better to look at the [Postgis raster documentation](https://postgis.net/docs/using_raster_dataman.html).

If you select **File system raster (out-of-db)**, in Advanced Options, the rasters 
will be stored on the server, on a folder `raster2postgis` inside the Postgresql `data_directory`.

You can check `data_directory` folder with:

```sql
# show data_directory;
       data_directory        
-----------------------------
 /var/lib/postgresql/11/main
```

You can also use `select setting from pg_settings where name = 'data_directory'` to find out the `data_directory`.

In this case, all out-of-db rasters will be stored in `/var/lib/postgresql/11/main/raster2postgis`. 

### Backup the database

If you use out-of-db rasters, please also backup the `raster2postgis` folder under the `data_directory`.

## Limitations

This plugin does not work against PostgreSQL installed on Windows. 
We need further investigation how to call `raster2pgsql` on the Windows side. 

## Feedback

Comments, issues and feature requests are welcome!

Go the the [raster2postgis issue tracker](https://gitlab.com/geomaster/raster2postgis/issues) and add you feedback or contribution.