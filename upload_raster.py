# -*- coding: utf-8 -*-
"""
/***************************************************************************
 raster2postgis
 QGIS plugin to import raster files to a postgis database
                              -------------------
        begin                : 2019-09-20
        copyright            : (C) 2019 by Geomaster
        email                : geral@geomaster.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2 as     *
 *   published by the Free Software Foundation.                            *
 *                                                                         *
 ***************************************************************************/
"""
import psycopg2
from psycopg2 import sql
import psycopg2.extras
from psycopg2.extensions import lobject

import os


def read_in_chunks(file_object, chunk_size=1024):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 1k."""
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data


class PostgisUtils:
    def __init__(self, parent, conString):
        self.parent = parent
        self.conString = conString

    def read_db_schemas(self):
        """Create, open and read schemas from database
        and close connection"""
        conn = None
        schemas = []
        try:
            conn = psycopg2.connect(self.conString)
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            getSchemas = """WITH "names"("name") AS (
                          SELECT n.nspname AS "name"
                            FROM pg_catalog.pg_namespace n
                              WHERE n.nspname !~ '^pg_'
                                AND n.nspname <> 'information_schema'
                        ) SELECT "name",
                          pg_catalog.has_schema_privilege(current_user, "name", 'CREATE') AS "create",
                          pg_catalog.has_schema_privilege(current_user, "name", 'USAGE') AS "usage"
                            FROM "names"
                         where pg_catalog.has_schema_privilege(current_user, "name", 'CREATE') and 
                         pg_catalog.has_schema_privilege(current_user, "name", 'USAGE')
                         order by 1;"""
            cur.execute(getSchemas)
            rows = cur.fetchall()
            for row in rows:
                schemas.append(row["name"])
                # self.parent.plainTextEdit.appendPlainText(
                #     "Schema: {0}\n".format(row["Name"]))
        except (Exception, psycopg2.Error) as error:
            raise ValueError("Error while connecting to PostgreSQL {0}".format(error))
        finally:
            if (conn):
                cur.close()
                conn.close()
            return schemas

    def checkPostgisRasterSupport(self):
        conn = None
        result = {}
        try:
            conn = psycopg2.connect(self.conString)
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute("select usesuper, CURRENT_USER from pg_user where usename = CURRENT_USER;")
            row = cur.fetchone()
            if row["usesuper"]:
                self.parent.plainTextEdit.appendPlainText("You are connect as {0} with superuser privileges".format(row["current_user"]))
                result["superuser"] = True
            else:
                self.parent.plainTextEdit.appendPlainText("You are connect as {0} without superuser privileges".format(row["current_user"]))
                result["superuser"] = False

            # check postgis
            cur.execute("select exists(select 1 from pg_extension where extname = 'postgis');")
            row = cur.fetchone()
            if row["exists"]:
                self.parent.plainTextEdit.appendPlainText("postgis extension exists")
                result["postgis"] = True
            else:
                self.parent.plainTextEdit.appendPlainText("postgis extension does not exist")
                result["postgis"] = False

            # check postgis_raster
            cur.execute("select exists(select 1 from pg_extension where extname = 'postgis_raster');")
            row = cur.fetchone()
            if row["exists"]:
                self.parent.plainTextEdit.appendPlainText("postgis_raster extension exists")
                result["postgis_raster"] = True
            else:
                self.parent.plainTextEdit.appendPlainText("postgis_raster extension does not exist")
                result["postgis_raster"] = False

            # check server OS
            cur.execute("select setting from pg_settings where name = 'dynamic_shared_memory_type';")
            row = cur.fetchone()
            if row["setting"] is not None and row["setting"] == 'posix':
                self.parent.plainTextEdit.appendPlainText("Postgresql server is posix (not Windows)")
                result["posix"] = True
            else:
                self.parent.plainTextEdit.appendHtml("Postgresql server is windows (<b>not supported</b>)")
                result["posix"] = False

            # check postgis raster support
            # first call ST_GDALDrivers() before check postgis.enable_outdb_rasters or postgis.gdal_enabled_drivers
            drivers = []
            cur.execute("SELECT short_name FROM ST_GDALDrivers();")
            rows = cur.fetchall()
            # self.parent.plainTextEdit.appendPlainText("{0} GDAL raster drivers available".format(len(rows)))
            for row in rows:
                drivers.append(row["short_name"])
            if len(rows) > 7:
                self.parent.plainTextEdit.appendPlainText("{0} GDAL raster drivers available: {1},...".format(len(rows), ",".join(drivers[:7])))
            else:
                self.parent.plainTextEdit.appendPlainText("{0} GDAL raster drivers available: {1}".format(len(rows), ",".join(drivers)))

            cur.execute("SHOW postgis.gdal_enabled_drivers;")
            row = cur.fetchone()
            if row["postgis.gdal_enabled_drivers"] is not None:
                self.parent.plainTextEdit.appendPlainText("postgis.gdal_enabled_drivers = {0}".format(row["postgis.gdal_enabled_drivers"]))
                if row["postgis.gdal_enabled_drivers"] == "DISABLE_ALL":
                    result["gdal_enabled_drivers"] = False
                else:
                    result["gdal_enabled_drivers"] = True

            cur.execute("SHOW postgis.enable_outdb_rasters;")
            row = cur.fetchone()
            if row["postgis.enable_outdb_rasters"] is not None:
                self.parent.plainTextEdit.appendPlainText("postgis.enable_outdb_rasters = {0}".format(row["postgis.enable_outdb_rasters"]))
                if row["postgis.enable_outdb_rasters"] == "on":
                    result["enable_outdb_rasters"] = True
                else:
                    result["enable_outdb_rasters"] = False

            cur.execute("select setting from pg_settings where name = 'data_directory';")
            row = cur.fetchone()
            if row is not None:
                result["data_directory"] = row["setting"]
                self.parent.plainTextEdit.appendPlainText(
                    "Server folder for out-of-db rasters: {0}/raster2postgis".format(row["setting"]))
            else:
                self.parent.plainTextEdit.appendPlainText(
                    "Server folder for out-of-db rasters: {0}".format("Not disclosed for non superuser roles"))
            # check raster2pgsql
            # this test requires superuser privileges :-(
            if result["superuser"]:
                try:
                    cur.execute("create temp table if not exists temp_table (line text);")
                    cur.execute("COPY temp_table FROM PROGRAM 'raster2pgsql';")
                    cur.execute("select line from temp_table where line like 'RELEASE: %';")
                    row = cur.fetchone()
                    self.parent.plainTextEdit.appendPlainText("raster2pgsql: {0}".format(row["line"]))
                except (Exception, psycopg2.Error) as error:
                    self.parent.plainTextEdit.appendPlainText("raster2pgsql: {0}".format("Not detected"))
            else:
                # check if functions exist
                result["plsqlfunctions"] = True
                try:
                    cur.execute("select 'public.r2pg_raster2pgsql_func'::regproc;")
                    row_r2pg_raster2pgsql_func = cur.fetchone()
                    cur.execute("select 'public.r2pg_proc_func'::regproc;")
                    row_r2pg_proc_func = cur.fetchone()
                    cur.execute("select 'public.r2pg_stor_func'::regproc;")
                    row_r2pg_stor_func = cur.fetchone()
                    if row_r2pg_raster2pgsql_func is not None and row_r2pg_proc_func is not None and row_r2pg_stor_func is not None:
                        self.parent.plainTextEdit.appendPlainText(
                            "PLSQL Functions for non superuser roles available")
                    if row_r2pg_raster2pgsql_func is not None:
                        check = """select version,present from r2pg_raster2pgsql_func() 
                        AS (version TEXT, present BOOL);
                        """
                        cur.execute(check)
                        row = cur.fetchone()
                        if row["version"] is not None:
                            self.parent.plainTextEdit.appendPlainText("raster2pgsql: {0}".format(row["version"]))

                except (Exception, psycopg2.Error) as error:
                    self.parent.plainTextEdit.appendPlainText("Error while check if functions exist {0}".format(error))
                    result["plsqlfunctions"] = False
                    self.parent.plainTextEdit.appendPlainText("PLSQL Functions for non superuser roles not available")

        except (Exception, psycopg2.Error) as error:
            self.parent.plainTextEdit.appendPlainText("Error while connecting to PostgreSQL {0}".format(error))
            raise ValueError("Error while connecting to PostgreSQL {0}".format(error))
        finally:
            if (conn):
                cur.close()
                conn.close()
            return result

class PostgisUploader:
    def __init__(self, parent, file_path, conString, outSchema, outTable, epsg, pepsg, tileSize, overviews, remotePath, isExternal, uploadList):
        self.parent = parent
        self.outSchema = outSchema
        self.outTable = outTable
        self.logTable = 'r2pg_log'
        self.rasterFolder = 'raster2postgis'

        self.file = file_path
        self.uploadList = uploadList
        self.isExternal = isExternal

        self.lobjects = []
        self.remotePath = remotePath
        self.rfile = None
        self.uid = None

        self.epsg = epsg
        self.pepsg = pepsg
        self.tileSize = tileSize
        self.overviews = overviews

        self.conn = psycopg2.connect(conString)
        self.cur = self.conn.cursor()

        self.callFunction = False
        self.checkContext()

    def checkContext(self):
        # check if superuser
        try:
            self.cur.execute(
                "select usesuper from pg_user where usename = CURRENT_USER;")
            isSuper = self.cur.fetchone()[0]
        except Exception as e:
            self.conn.rollback()
            raise(e)

        # check if functions exist
        procFuncs = True
        try:
            self.cur.execute("select 'public.r2pg_raster2pgsql_func'::regproc;")
            self.cur.execute("select 'public.r2pg_proc_func'::regproc;")
            self.cur.execute("select 'public.r2pg_stor_func'::regproc;")
        except Exception:
            self.conn.rollback()
            procFuncs = False

        # check folder
        folder = True
        # try:
        #     self.cur.execute("COPY (SELECT 1) TO PROGRAM 'ls %s';", (self.rasterFolder,))
        # except Exception:
        #     self.conn.rollback()
        #     folder = False

        # check schema
        schema = False
        try:
            self.cur.execute(
                "select exists(select 1 from pg_namespace where nspname = %s);", (self.outSchema,))
            if self.cur.fetchone()[0] is True:
                schema = True
        except Exception as e:
            self.conn.rollback()
            raise(e)

        # check postgis
        postgis = False
        try:
            self.cur.execute(
                "select exists(select 1 from pg_extension where extname = 'postgis');")
            if self.cur.fetchone()[0] is True:
                postgis = True
        except Exception as e:
            self.conn.rollback()
            raise(e)

        # setup context if necessary
        # if isSuper and (not procFuncs or not schema or not postgis):
        #    self.setupDB()

        # throw exception if not superuser and faulty context
        # if not folder:
        #    raise ValueError('Destination folder does not exist.')

        # throw exception if not superuser and faulty context
        if not isSuper and (not procFuncs or not schema or not postgis):
            msgf = 'Unable to run process. ' if not procFuncs else ''
            msgs = 'Unable to create schema. ' if not schema else ''
            msgp = 'Unable to setup postgis. ' if not schema else ''
            raise ValueError(
                msgs + msgf + msgp + 'Please use a connection as superuser first for setup')

        if not isSuper and (procFuncs and schema):
            self.callFunction = True

    def setupDB(self):
        # pec = "create extension if not exists postgis;"
        # csc = "create schema if not exists {};"
        #
        # Syntax for dropping all functions with the same name and different signatures works on 10 and up
        # Not supported in Postgres 9.6
        r2pg_raster2pgsql_func = """drop function IF EXISTS public.r2pg_raster2pgsql_func;
            CREATE OR REPLACE FUNCTION public.r2pg_raster2pgsql_func()
             RETURNS RECORD
             LANGUAGE plpgsql
             SECURITY DEFINER
            AS $function$
                declare
                    ret RECORD;
                begin
                    create temp table if not exists temp_table (line text);
                    COPY temp_table FROM PROGRAM 'raster2pgsql';
                    select line as version, true as present INTO ret from temp_table where line like 'RELEASE: %';
                   RETURN ret;
                end;
            $function$
            ;"""
        sfc = (
            "drop function IF EXISTS r2pg_stor_func;\n"
            "create function r2pg_stor_func(integer, text, boolean, integer) returns boolean as $r2pg_stor_func$\n"
            "    declare\n"
            "        loid integer := $1;\n"
            "        fpath text := $2;\n"
            "        persist boolean := $3;\n"
            "        grouploid integer := $4;\n"
            "        dpath text;\n"
            "    begin\n"
            "        if persist is TRUE then\n"
            "            dpath := (select setting from pg_settings where name = 'data_directory');\n"
            "            dpath := dpath || '/raster2postgis/' || (grouploid % 1000)::text || '/';\n"
            "            execute format('COPY (SELECT 1) TO PROGRAM ''mkdir -p %1$s''', dpath);\n"
            "            execute format('select lo_export(%1$s::oid, %2$L)', loid, dpath || fpath);\n"
            "            return TRUE;\n"
            "        else\n"
            "            execute format('select lo_export(%1$s::oid, %2$L)', loid, fpath);\n"
            "            return TRUE;\n"
            "        end if;\n"
            "    end;\n"
            "$r2pg_stor_func$ language plpgsql security definer;"
        )
        pfc = (
            "drop function IF EXISTS r2pg_proc_func;\n"
            "create function r2pg_proc_func(text, text, text, text, text, text, text, text, text, text, text, boolean, text) returns boolean as $r2pg_stor_func$\n"
            "    declare\n"
            "        outSchema text := $1;\n"
            "        logTable text := $2;\n"
            "        outTable text := $3;\n"
            "        epsg text := $4;\n"
            "        pepsg text := $5;\n"
            "        rfile text := $6;\n"
            "        gdalWarpOut text := $7;\n"
            "        tileSize text := $8;\n"
            "        overviews text := $9;\n"
            "        extFlag text := $10;\n"
            "        dbname text := $11;\n"
            "        persist boolean := $12;\n"
            "        oid text := $13;\n"
            "        dpath text;\n"
            "        overview_table text;\n"
            "    begin\n"
            "        if persist is TRUE then\n"
            "            dpath := (select setting from pg_settings where name = 'data_directory');\n"
            "            dpath := dpath || '/raster2postgis/' || oid || '/';\n"
            "        else\n"
            "            dpath := '';\n"
            "        end if;\n"
            "        execute format('create table if not exists %1$s.%2$s(log varchar)', outSchema, logTable);\n"
            "        execute format('truncate table %1$s.%2$s', outSchema, logTable);\n"
            "        execute format('COPY %1$s.%2$s FROM PROGRAM '' gdalwarp -s_srs %3$s -t_srs %4$s -of vrt ''%5$L'' /vsistdout/"
            " | gdal_translate -co compress=lzw -co PREDICTOR=2 /vsistdin/ ''%6$L''''', outSchema, logTable, epsg, pepsg, dpath || rfile, dpath || gdalWarpOut);\n"
            "        execute format('COPY %1$s.%2$s FROM PROGRAM '' raster2pgsql -s %3$s -d -I -C -F -M -t %4$s -l %5$s %6$s ''%7$L'' %8$s.%9$s"
            " | psql %10$s''', outSchema, logTable, pepsg, tileSize, overviews, extFlag, dpath || gdalWarpOut, outSchema, outTable, dbname);\n"
            "        execute format('ALTER TABLE \"%1$s\".\"%2$s\" OWNER TO %3$s', outSchema, outTable, session_user);\n"            
            "        FOR overview_table IN SELECT o_table_name FROM public.raster_overviews WHERE r_table_schema = outSchema and r_table_name = outTable\n"
            "        loop\n"
    	    "           EXECUTE format('ALTER TABLE \"%1$s\".\"%2$s\" OWNER TO %3$s;', outSchema, overview_table, session_user);\n"
            "        END LOOP;\n"
            "        return TRUE;\n"
            "    end;\n"
            "$r2pg_stor_func$ language plpgsql security definer;"
        )
        # self.cur.execute(pec)
        # self.cur.execute(sql.SQL(csc).format(sql.Identifier(self.outSchema)))
        self.cur.execute(r2pg_raster2pgsql_func)
        self.cur.execute(sfc)
        self.cur.execute(pfc)
        self.conn.commit()

    def storeRaster(self):
        if not self.callFunction and self.isExternal:
            self.cur.execute("show data_directory;")
            self.remotePath = self.cur.fetchone()[0] + '/raster2postgis/'

        for f in self.uploadList:
            with open(f, 'rb') as fd:
                # Initate the session with postgresql to write large object instance
                lobj = lobject(self.conn, 0, 'w')
                # Write the data to database
                for piece in read_in_chunks(fd, 1024000):
                    lobj.write(piece)
                # Commit transaction
                self.conn.commit()
                self.lobjects.append(lobj.oid)

                # get uid for remote file names
                if self.uid is None:
                    self.uid = str(lobj.oid)
                    if not self.callFunction and self.isExternal:
                        self.remotePath = self.remotePath + str(lobj.oid % 1000) + '/'
                        self.cur.execute("COPY (SELECT 1) TO PROGRAM 'mkdir -p {0}';".format(self.remotePath))
                rfile = self.remotePath + self.uid + \
                    '_' + os.path.basename(f)
                # keep reference to main raster file
                if os.path.basename(f) == os.path.basename(self.file):
                    self.rfile = rfile

                # call export
                # Multiple files can be export
                # They should be stored on the same folder for out-of-db rasters
                if not self.callFunction:
                    self.cur.execute(
                        "select lo_export(%s::oid, %s)", (lobj.oid, rfile))
                else:
                    self.cur.execute(
                        "select r2pg_stor_func(%s, %s, %s, %s)", (lobj.oid, rfile, self.isExternal, int(self.uid)))

    def raster2pgsql(self):
        # TODO: check if table already exists
        # TODO: check if connection as postgres was possible, if not ask for credentials

        self.parent.write("Server temporary file: {0}".format(self.rfile))

        gdalWarpOut = os.path.splitext(self.rfile)[0] + '_p.tif'
        extFlag = '-R' if self.isExternal else ''

        self.parent.write("Server intermediate file: {0}".format(gdalWarpOut))

        if not self.callFunction:
            # self.parent.write("raster2pgsql: not function based")
            self.cur.execute(sql.SQL("create table if not exists {}.{}(log varchar)").format(
                sql.Identifier(self.outSchema), sql.Identifier(self.logTable)))
            self.cur.execute(sql.SQL("truncate table {}.{}").format(
                sql.Identifier(self.outSchema), sql.Identifier(self.logTable)))
            self.conn.commit()

            # call gdalwarp
            cmdGd = sql.SQL(
                "COPY {}.{} FROM PROGRAM ' gdalwarp -s_srs " +
                self.epsg + " -t_srs " + self.pepsg + " -of vrt ''" + self.rfile
                + "'' /vsistdout/ | gdal_translate -co compress=lzw -co PREDICTOR=2 /vsistdin/ ''" + gdalWarpOut + "'''").format(
                sql.Identifier(self.outSchema), sql.Identifier(self.logTable))
            # print(cmdGd)
            self.cur.execute(cmdGd)
            try:
                if int(self.cur.statusmessage.split()[-1]) == 0:
                    raise ValueError(
                        'Problem processing raster on the database')
            except Exception as e:
                raise (e)

            # call raster2pgsql
            cmd = sql.SQL(
                "COPY {}.{} FROM PROGRAM ' raster2pgsql -s " +
                self.pepsg + " -d -I -C -F -M -t " + self.tileSize
                + " -l " + self.overviews + ' ' + extFlag + " ''" + gdalWarpOut + "'' {}.{} | psql ''" +
                self.conn.get_dsn_parameters()['dbname'] + "'''").format(
                    sql.Identifier(self.outSchema), sql.Identifier(self.logTable), sql.Identifier(self.outSchema), sql.Identifier(self.outTable))
            # print(cmd)
            self.cur.execute(cmd)
            try:
                if int(self.cur.statusmessage.split()[-1]) == 0:
                    raise ValueError(
                        'Problem importing raster on the database')
            except Exception as e:
                raise (e)

            # give proper ownership
            owner = """DO $do$
                DECLARE
                    overview_table text;
                begin
                    EXECUTE format('ALTER TABLE "%%1$s"."%%2$s" OWNER TO %%3$s;', %(schema)s, %(table)s, current_user );
                    FOR overview_table IN SELECT o_table_name FROM public.raster_overviews WHERE r_table_schema = %(schema)s and r_table_name = %(table)s
                    loop
                        EXECUTE format('ALTER TABLE "%%1$s"."%%2$s" OWNER TO %%3$s;', %(schema)s, overview_table, current_user );
                    END LOOP;
                END;
                $do$;"""
            try:
                self.cur.execute(owner, {'schema': self.outSchema, 'table': self.outTable})
            except Exception as e:
                raise (e)

        else:
            self.parent.write("Calling postgresql server functions for non superuser account")
            self.cur.execute("select r2pg_proc_func(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                             (self.outSchema, self.logTable, self.outTable, self.epsg, self.pepsg, self.rfile, gdalWarpOut,
                              self.tileSize, self.overviews, extFlag, self.conn.get_dsn_parameters()['dbname'], self.isExternal, str(int(self.uid) % 1000)))
            try:
                if not self.cur.fetchone()[0] == True:
                    raise ValueError(
                        'Problem importing raster on the database')
            except Exception as e:
                raise (e)

    def doRollback(self):
        if self.conn:
            self.conn.rollback()

    def cancel(self):
        if self.conn:
            self.conn.cancel()

    def exit(self):
        # TODO
        # make sure connection is still valid
        for oid in self.lobjects:
            self.cur.execute("select lo_unlink(%s::oid)", (oid,))

        self.conn.commit()
        self.cur.close()
        self.conn.close()
